#!/usr/bin/env bash

comment=$@
git commit -m "$comment"
curr_branch=`git rev-parse --abbrev-ref HEAD`
remote_name=`git remote`
git push $remote_name $curr_branch
