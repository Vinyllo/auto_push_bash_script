# Auto_push_bash_script

Scrypt add commit message without any flags, and push it on current
remote from current local repository branch name<br />
Commit messege is first arg of script <br />
No need of "" signs<br />
Remote and branch are recognizing automatically<br />

Usage: <br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;	sh auto_push.sh COMMIT_MESSEGE


To make scrypt working comfortable:<br />
&nbsp; &nbsp; &nbsp; &nbsp;	1. add script to home directory<br />
&nbsp; &nbsp; &nbsp; &nbsp; 2. add alias in .bashrc file <br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;		i.e: alias ap="sh auto_push.sh"<br />
&nbsp; &nbsp; &nbsp; &nbsp; 	3. reload terminals<br />
&nbsp; &nbsp; &nbsp; &nbsp; 	Usage after that:<br />
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;	ap COMMIT_MESSEGE<br />

File had to be added on stage area

Feel free sending messages to me, about errors and improvements that I can
implement
